# 一 综述
&emsp;九大排序排序是数据结构体系中最重要的内容之一，这一块必须要非常熟练的掌握，应该做到可以立马写出每个排序的代码，有多种实现方法的必须多种都能很快写出来，当然对各个排序的性能的了解也是基础且重要的。我们先对排序这一块进行一个整体的把握。  

![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/排序类别.png "")  
<center>图 1.1 排序类别</center>  

![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/算法复杂度列表.png "")  
<center>图 1.2 算法复杂度列表</center>

# 二 概念
**内排序**：在对待排序数据存放在内存中进行的排序过程。是我们主要讨论与学习的重点。  
**外排序**：待排数据量太大，无法一次性将所有待排序数据放入内存中，在排序过程中需要对磁盘等外部储存器进行访问。不是我们谈论与学习的重点，但也要通过相关资料或书籍了解其基本步骤。  
**比较排序**：排序过程中需要对数据关键字进行比较。  
**非比较排序**：排序过程中不需要对数据关键字进行比较。  
排序算法的稳定性：在每一次单趟排序后，相同关键字的相对顺序不变。  

稳定的排序算法：冒泡排序、插入排序、归并排序和基数排序。  
不是稳定的排序算法：选择排序、快速排序、希尔排序、堆排序。  

# 三 方法

## 3.1 冒泡排序（BUBBLE SORT）
&emsp;冒泡排序是一种简单的排序算法。它重复地走访过要排序的数列，一次比较两个元素，如果它们的顺序错误就把它们交换过来。走访数列的工作是重复地进行直到没有再需要交换，也就是说该数列已经排序完成。这个算法的名字由来是因为越小的元素会经由交换慢慢“浮”到数列的顶端。 

**步骤**
1.比较相邻的元素。如果第一个比第二个大，就交换他们两个；  
2.对每一对相邻元素作同样的工作，从开始第一对到结尾的最后一对。这步做完后，最后的元素会是最大的数； 
3.针对所有的元素重复以上的步骤，除了最后一个；  
4.持续每次对越来越少的元素重复上面的步骤，直到没有任何一对数字需要比较。 

**动图**
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/冒泡排序.gif "Wizard-Diagrams")  
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/冒泡排序1.gif "Wizard-Diagrams")  
<center>图 3.1 冒泡排序</center>

**代码**
```python
def BubbleSort(arr):
    for i in range(len(arr)):
        flag = 0 # 设置flag，若本身已经有序，则直接break
        for j in range(len(arr) - i - 1):
            if arr[j] > arr[j + 1]:
                flag = 1
                arr[j], arr[j+1] = arr[j+1], arr[j]
        if flag == 0: 
            break
    return arr
```
## 3.2 交换排序

**步骤**
1.从数列中挑出一个元素，称为 “基准”（pivot）;  
2.重新排序数列，所有元素比基准值小的摆放在基准前面，所有元素比基准值大的摆在基准的后面（相同的数可以到任一边）。在这个分区退出之后，该基准就处于数列的中间位置。这个称为分区（partition）操作；  
3.递归地（recursive）把小于基准值元素的子数列和大于基准值元素的子数列排序。  

**动图**
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/交换排序.gif "")  
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/交换排序1.gif "")  
<center>图 3.2 交换排序</center>

**代码**
```python
def QuickSort(arr):
    if len(arr) <= 1:
        return arr
    mid, left, right = arr[0], [], []
    for i in arr[1:]:
        if i < mid:
            left.append(i)
        else:
            right.append(i)
    return QuickSort(left) + [mid] + QuickSort(right)
```

## 3.3 选择排序
&emsp;首先在未排序序列中找到最小（大）元素，存放到排序序列的起始位置，然后，再从剩余未排序元素中继续寻找最小（大）元素，然后放到已排序序列的末尾。以此类推，直到所有元素均排序完毕。 

**步骤**
1.首先在未排序序列中找到最小（大）元素，存放到排序序列的起始位置；
2.再从剩余未排序元素中继续寻找最小（大）元素，然后放到已排序序列的末尾；
3.重复第二步，直到所有元素均排序完毕。 

**动图**
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/选择排序.gif "")  
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/选择排序1.gif "")  
<center>图 3.3 选择排序 </center>

**代码**
```python
def SelectSort(arr):
    for i in range(len(arr) - 1):
        ind = i
        for j in range(i + 1, len(arr)):
            if arr[ind] > arr[j]:
                ind = j
        arr[i], arr[ind] = arr[ind], arr[i]
    return arr
```
## 3.4 堆排序
堆排序（Heapsort）是指利用堆这种数据结构所设计的一种排序算法。堆积是一个近似完全二叉树的结构，并同时满足堆积的性质：即子结点的键值或索引总是小于（或者大于）它的父节点。

**步骤**
1.创建一个堆 $ H[0 ... n - 1]$；
2.把堆首（最大值）和堆尾互换；
3.把堆的尺寸缩小 1，并调用 shift_down(0)，目的是把新的数组顶端数据调整到相应位置；
4.重复步骤 2，直到堆的尺寸为 1。

**动图**
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/堆排序.gif "")  
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/堆排序1.gif "")  
<center>图 3.4 堆排序</center>

**代码**
```python
def Build(arr, root, end):
    while True:
        child = 2 * root + 1 # 由于堆是一个完全二叉树，所以可以根据其特性，推算出左子节点的位置
        if child > end: # 若左子节点超过了最后一个节点，则终止循环
            break
        if (child + 1 <= end) and (arr[child + 1] > arr[child]): # 若右子节点在最后一个节点之前，并且右子节点比左子节点大，则我们的孩子指针移到右子节点上
            child += 1
        if arr[child] > arr[root]: # 若最大的孩子节点大于根节点，则交换两者顺序，并且将根节点指针，移到这个孩子节点上
            arr[child], arr[root] = arr[root], arr[child]
            root = child
        else:
            break
    return arr

def HeapSort(arr):
    n = len(arr)
    first_root = n // 2 - 1 # 首先根据完全二叉树的性质，确认最深最后的那个根节点的位置
    for root in range(first_root, -1, -1): # 由后向前遍历所有的根节点，建堆并进行调整
        Build(arr, root, n - 1)
        
    for end in range(n - 1, 0, -1): # 调整完成后，将堆顶的根节点与堆内最后一个元素调换位置，此时为数组中最大的元素，然后重新调整堆，将最大的元素冒到堆顶。依次重复上述操作
        arr[0], arr[end] = arr[end], arr[0]
        Build(arr, 0, end - 1)
    return arr
    
HeapSort(q)

```

## 3.5 插入排序
&emsp;通过构建有序序列，对于未排序数据，在已排序序列中从后向前扫描，找到相应位置并插入。  
**步骤**
1.将第一待排序序列第一个元素看做一个有序序列，把第二个元素到最后一个元素当成是未排序序列； 
2.从头到尾依次扫描未排序序列，将扫描到的每个元素插入有序序列的适当位置（如果待插入的元素与有序序列中的某个元素相等，则将待插入元素插入到相等元素的后面）。

**动图**
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/插入排序.gif "")  
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/插入排序1.gif "")  
<center>图 3.5 插入排序 </center>

**代码**
```python

```
## 3.6 希尔排序
&emsp;1959年Shell发明，第一个突破O(n2)的排序算法，是简单插入排序的改进版。它与插入排序的不同之处在于，它会优先比较距离较远的元素。希尔排序又叫缩小增量排序。

**步骤**
1.选择一个增量序列$t1,t2,...,tk，$ 其中 $ ti > tj, tk = 1$；
2.按增量序列个数$k$，对序列进行$k$趟排序；
3.每趟排序，根据对应的增量$ti$，将待排序列分割成若干长度为$m$的子序列，分别对各子表进行直4.接插入排序。仅增量因子为1时，整个序列作为一个表来处理，表长度即为整个序列的长度。

**动图**
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/希尔排序.gif "")  
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/希尔排序1.gif "")  
<center>图 3.6 希尔排序 </center>

**代码**
```python

```
## 3.7 归并排序
归并排序是建立在归并操作上的一种有效的排序算法。该算法是采用分治法（Divide and Conquer）的一个非常典型的应用。将已有序的子序列合并，得到完全有序的序列；即先使每个子序列有序，再使子序列段间有序。若将两个有序表合并成一个有序表，称为2-路归并。   

**步骤**
1.申请空间，使其大小为两个已经排序序列之和，该空间用来存放合并后的序列；  
2.设定两个指针，最初位置分别为两个已经排序序列的起始位置；  
3.比较两个指针所指向的元素，选择相对小的元素放入到合并空间，并移动指针到下一位置；  
4.重复步骤 3 直到某一指针达到序列尾；  
5.将另一序列剩下的所有元素直接复制到合并序列尾。  
**分析**
**动图**
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/归并排序.gif "")  
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/归并排序1.gif "")  
<center>图 3.7 归并排序 </center>

**代码**
```python
def Merge(arr1, arr2): # 可参考有序链表的合并
    result = []
    while arr1 and arr2:
        if arr1[0] < arr2[0]:
            result.append(arr1.pop(0))
        else:
            result.append(arr2.pop(0))
    if arr1:
        result += arr1
    if arr2:
        result += arr2
    return result

def MergeSort(arr):
    if len(arr) <= 1:
        return arr
    mid = len(arr) // 2
    return Merge(MergeSort(arr[:mid]), MergeSort(arr[mid:]))

MergeSort(q)

```
## 3.8 基数排序
基数排序是按照低位先排序，然后收集；再按照高位排序，然后再收集；依次类推，直到最高位。有时候有些属性是有优先级顺序的，先按低优先级排序，再按高优先级排序。最后的次序就是高优先级高的在前，高优先级相同的低优先级高的在前。 

**步骤**
1.将所有待比较数值（正整数）统一为同样的数位长度，数位较短的数前面补零；
2.从最低位开始，依次进行一次排序；
3.从最低位排序一直到最高位排序完成以后, 数列就变成一个有序序列。

**动图**
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/基数排序.gif "")  
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/基数排序1.gif "")  
<center>图 3.8 基数排序 </center>

**代码**
```python

```

## 3.9 计数排序
计数排序不是基于比较的排序算法，其核心在于将输入的数据值转化为键存储在额外开辟的数组空间中。 作为一种线性时间复杂度的排序，计数排序要求输入的数据必须是有确定范围的整数。

**步骤**
1.花O(n)的时间扫描一下整个序列 A，获取最小值 min 和最大值 max；  
2.开辟一块新的空间创建新的数组 B，长度为 ( max - min + 1)；  
3.数组 B 中 index 的元素记录的值是 A 中某元素出现的次数；  
4.最后输出目标整数序列，具体的逻辑是遍历数组 B，输出相应元素以及对应的个数。  

**动图**
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/计数排序.gif "")  
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/计数排序1.gif "")  
<center>图 3.9 计数排序 </center>

**代码**
```python

```
## 3.10 桶排序
桶排序是计数排序的升级版。它利用了函数的映射关系，高效与否的关键就在于这个映射函数的确定。桶排序 (Bucket sort)的工作的原理：假设输入数据服从均匀分布，将数据分到有限数量的桶里，每个桶再分别排序（有可能再使用别的排序算法或是以递归方式继续使用桶排序进行排）。

**步骤**
1.设置固定数量的空桶;  
2.把数据放到对应的桶中;  
3.对每个不为空的桶中数据进行排序;  
4.拼接不为空的桶中数据，得到结果。  

**动图**
![](https://gitlab.com/blogs_prj/algorithm_blogs/raw/master/sort/res/桶排序.gif "")  
<center>图 3.10 桶排序 </center>

**代码**
```python

```

参考文献
[1] https://www.cnblogs.com/onepixel/articles/7674659.html
[2] https://blog.csdn.net/weixin_41929524/article/details/87971448
[3] 五分钟学算法